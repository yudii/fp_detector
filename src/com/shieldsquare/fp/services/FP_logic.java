package com.shieldsquare.fp.services;

import java.util.ArrayList;

import com.shieldsquare.fp.model.IPDetails;

public interface FP_logic {
	public ArrayList<IPDetails> getIP_details(int sid,String startD,String endD);
}
