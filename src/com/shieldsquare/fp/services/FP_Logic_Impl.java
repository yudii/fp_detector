package com.shieldsquare.fp.services;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import javax.naming.NamingException;
import com.shieldsquare.fp.dbutils.DButils;
import com.shieldsquare.fp.model.IPDetails;

public class FP_Logic_Impl implements FP_logic {

	Connection con = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	public ArrayList<IPDetails> getIP_details(int sid, String startD,
			String endD) {

		ArrayList<IPDetails> ipDetails_List = new ArrayList<IPDetails>();
		try {
			//System.out.println("Yogesh");
			con = DButils.getMySQLConnection();
			ps = (PreparedStatement) con
					.prepareStatement("select id.ip_address,id.country_name,id.city_name,id.isp,id.domain,sum(total_requests) as req from ip_analysis ia,ip_details id where dt >= ? and dt <= ? and id.id=ia.ip_address and sid=? group by id.ip_address,id.country_name,id.city_name,id.isp,id.domain order by req desc");
			ps.setString(1, startD);
			ps.setString(2, endD);
			ps.setInt(3, sid);
			rs = ps.executeQuery();
			while (rs.next()) {
				IPDetails ipd = new IPDetails();
				String ip_add = rs.getString("ip_address");
				String city_Name = rs.getString("city_name");
				String country_Name = rs.getString("country_name");
				String isp_Name = rs.getString("isp");
				String domain_Name = rs.getString("domain");
				int total_Request = rs.getInt("req");
				PreparedStatement ps1 = (PreparedStatement) con
						.prepareStatement("select distinct(useragents)as ua from bot_ip_details where IP_ADDRESS= ? and DATE_AND_HOUR >= ? and DATE_AND_HOUR<= ? and sid= ?");

				ps1.setString(1, ip_add);
				ps1.setString(2, startD);
				ps1.setString(3, endD);

				ps1.setInt(4, sid);
				ResultSet rs1 = ps1.executeQuery();
				StringBuilder useragents = new StringBuilder();
				while (rs1.next()) {
					String useragent = rs1.getString("ua");
					if (useragent != null) {
						useragents.append(useragent);
						useragents.append(" ; ");
						// System.out.println("User agent is: " + useragent);
					}
				}
				String useragentsString = useragents.toString();
				ipd.setIp_address(ip_add);
				ipd.setCity_name(city_Name);
				ipd.setCountry_name(country_Name);
				ipd.setIsp_name(isp_Name);
				ipd.setDomain_name(domain_Name);
				ipd.setTotal_request(total_Request);
				ipd.setUseragent(useragentsString);
				
				String nslookup = nslookup(ip_add);
				ipd.setNslookup(nslookup);
				ipDetails_List.add(ipd);
			}

		} catch (Exception e) {

		} finally {
			try {
				con.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return ipDetails_List;
	}

	public static String nslookup(String args) throws IOException,
			NamingException {
		String retVal = null;
		String ip = args;
		if (ip == null)
			return retVal;
		final String[] bytes = ip.split("\\.");
		if (bytes.length == 4) {
			try {
				final java.util.Hashtable<String, String> env = new java.util.Hashtable<String, String>();
				env.put("java.naming.factory.initial",
						"com.sun.jndi.dns.DnsContextFactory");
				final javax.naming.directory.DirContext ctx = new javax.naming.directory.InitialDirContext(
						env);
				final String reverseDnsDomain = bytes[3] + "." + bytes[2] + "."
						+ bytes[1] + "." + bytes[0] + ".in-addr.arpa";
				final javax.naming.directory.Attributes attrs = ctx
						.getAttributes(reverseDnsDomain,
								new String[] { "PTR", });

				for (final javax.naming.NamingEnumeration<? extends javax.naming.directory.Attribute> ae = attrs
						.getAll(); ae.hasMoreElements();) {

					final javax.naming.directory.Attribute attr = ae.next();
					final String attrId = attr.getID();

					for (final java.util.Enumeration<?> vals = attr.getAll(); vals
							.hasMoreElements();) {
						String value = vals.nextElement().toString();
						if ("PTR".equals(attrId)) {
							final int len = value.length();
							if (value.charAt(len - 1) == '.') {
								// Strip out trailing period
								value = value.substring(0, len - 1);
							}
							retVal = value;
						}
					}
				}
				ctx.close();
			} catch (final javax.naming.NamingException e) {
				// No reverse DNS that we could find, try with InetAddress
				retVal = "No reverse DNS found"; // NO-OP
			}
		}
		if (null == retVal) {
			try {
				retVal = java.net.InetAddress.getByName(ip)
						.getCanonicalHostName();
			} catch (final java.net.UnknownHostException e1) {
				retVal = ip;
			}
		}
		return retVal;
	}

}
