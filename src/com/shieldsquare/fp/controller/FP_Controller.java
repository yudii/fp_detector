package com.shieldsquare.fp.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.SimpleAttributeSet;

import org.apache.catalina.connector.Request;

import com.oracle.jrockit.jfr.RequestDelegate;
import com.shieldsquare.fp.model.IPDetails;
import com.shieldsquare.fp.services.FP_Logic_Impl;
import com.shieldsquare.fp.services.FP_logic;

/**
 * Servlet implementation class FP_Controller
 */
public class FP_Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		int sid = Integer.parseInt(request.getParameter("SID"));
		String startDate = request.getParameter("StartDate");
		String endDate = request.getParameter("EndDate");

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
		Date startdate = null;
		Date enddate = null;
		try {
			startdate = df.parse(startDate);
			enddate = df.parse(endDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		String startD = null;
		String endD = null;

		startD = sdf.format(startdate);
		endD = sdf.format(enddate);

		FP_Logic_Impl fp_logic = new FP_Logic_Impl();
		ArrayList<IPDetails> ipdetail = fp_logic.getIP_details(sid, startD,
				endD);

		RequestDispatcher rd = request
				.getRequestDispatcher("ipdetails_show.jsp");
		request.setAttribute("ip_list", ipdetail);
		rd.forward(request, response);

	}

}
