package com.shieldsquare.fp.model;

public class IPDetails {

	private String ip_address;
	private String city_name;
	private String country_name;
	private String isp_name;
	private String domain_name;
	private int total_request;
	private String useragent;
	private String nslookup;
	
	public String getNslookup() {
		return nslookup;
	}
	public void setNslookup(String nslookup) {
		this.nslookup = nslookup;
	}
	public String getIp_address() {
		return ip_address;
	}
	public String getUseragent() {
		return useragent;
	}
	public void setUseragent(String useragent) {
		this.useragent = useragent;
	}
	public void setIp_address(String ip_address) {
		this.ip_address = ip_address;
	}
	public String getCity_name() {
		return city_name;
	}
	public void setCity_name(String city_name) {
		this.city_name = city_name;
	}
	public String getCountry_name() {
		return country_name;
	}
	public void setCountry_name(String country_name) {
		this.country_name = country_name;
	}
	public String getIsp_name() {
		return isp_name;
	}
	public void setIsp_name(String isp_name) {
		this.isp_name = isp_name;
	}
	public String getDomain_name() {
		return domain_name;
	}
	public void setDomain_name(String domain_name) {
		this.domain_name = domain_name;
	}
	public int getTotal_request() {
		return total_request;
	}
	public void setTotal_request(int total_request) {
		this.total_request = total_request;
	}
	
	
	
	
	
}
