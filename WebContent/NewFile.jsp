<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<div class="header">
	<img src="shieldquare-logo.png" width="183" height="40" border="0"
		align="left" /><br> <br> <br>
</div>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
body {
	background: #F0F0F0 !important;
}

.clearfixed {
	clear: both;
}
.header {
	width: 100%;
	float: none;
	background-color: #FFFFFF;
	padding-left: 3%;
	padding-top: 0.5%;
	padding-bottom: 0%;
}

.btn {
	display: inline-block;
	padding: 6px 35px;
	margin-bottom: -10;
	background: cornflowerblue;
	line-height: 1.42857143;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
	margin-left: -92px;
	/* background-image: url("images.jpg");
	width:60% ;
	height: 40%; */
	/* background: red; */
}
.form-horizontal { /* width: 100%;
  color: #555;
  border: 1px solid #ccc;
  border-radius: 4px; */
	border: 1px #e4e4e4 solid;
	padding: 20px;
	border-radius: 4px;
	box-shadow: 0 0 6px #ccc;
	background-color: #fff;
	margin-right: 268px;
	margin-left: 268px;
}
.form-horizontal .form-group {
	margin-right: -120px;
	margin-left: -76px;
}
.container {
	margin-left: 52px;
	margin-bottom: 170px;
	padding-top: 28px;
	padding-left: 52px
}
</style>


</head>


<div class="modulename">
				<marquee direction="left" scrolldelay="150" class="alternate">

					<h2>
						<strong>False-Positive Checker</strong>
					</h2>

				</marquee>
			</div>
<body>

	<div class="container">

		<div class="row">
			<div class="col-md-12">
				
							<form class="form-horizontal" method="post" action="FP_Controller"
				id="form" onsubmit="return validateForm()">
				<div class="form-group">
					<label class="control-label col-xs-4" for="SID">SID:</label>
					<div class="col-xs-4" align="center">
						<div class="sid">
							<input type="text" class="form-control" id="SID"
								placeholder="Enter sid" name="SID" maxlength="4" autofocus required>
						</div>
					</div>
				</div>


				<div class="form-group">
					<label class="control-label col-xs-4" for="StartDate">Start
						Date:</label>
					<div class="col-xs-4">
						<input type="datetime-local" class="form-control" id="StartDate"
							name="StartDate">
					</div>
				</div>



				<div class="form-group">
					<label class="control-label col-xs-4" for="EndDate">End
						Date:</label>
					<div class="col-xs-4">
						<input type="datetime-local" class="form-control" id="EndDate"
							name="EndDate">
					</div>
				</div>

				<div class="form-group" align="center">
					<!-- <div class="col-sm-offset-2 col-sm-10"> -->
					<!-- <input type="image" src="images.jpg" alt="Submit" width="100" height="40"> -->

					<button type="submit" class="btn .btn-primary""background-color:red" >Hit
						me!</button>
				</div>
			</form>
				
				
				
				
				
			</div>
		</div>



		<div class="row">
		
		
			<div class="col-md-6">
					<h2>
				<font color="blue"><strong>Read before use</strong></font>
			</h2>
				<p>FP_Detector will help to find false positive for customer</p>
			<p>It will show IP information like</p>
			<ul>
				<li>IP Address</li>
				<li>ISP</li>
				<li>City</li>
				<li>Country</li>
				<li>NSLookup</li>
				<li>Distinct Useragent</li>
			</ul>
				
				
			</div>

			<div class="col-md-6">
			<h2>
				<font color="red"><strong>How to use</strong></font>
			</h2>
			<ul>
				<li>Enter customer internal SID</li>
				<li>Provide start and end date with time</li>
				<li>For best result use Google Chrome</li>
			</ul>
		
			</div>
		
		
		</div>


		<div class="row">
			<div class="col-md-12">
				<h1>Footer Side</h1>
			</div>
		</div>
	</div>


</body>
</html>